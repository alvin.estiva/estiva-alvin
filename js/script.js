
let addition = document.getElementById('add');
let subtract = document.getElementById('sub');
let multiplication = document.getElementById('mul');
let division = document.getElementById('div');
let modulo = document.getElementById('mod');

addition.addEventListener("click", function(){
	let num1 = document.getElementById("n1");
	let nm1 = parseInt(num1.value);
	let num2 = document.getElementById("n2");
	let nm2 = parseInt(num2.value);
	
let addi = nm1 + nm2;
document.getElementById('result').innerHTML = addi;
document.getElementById('operation').innerHTML = "Addition";
});

subtract.addEventListener("click", function(){
	let num1 = document.getElementById("n1");
	let nm1 = parseInt(num1.value);
	let num2 = document.getElementById("n2");
	let nm2 = parseInt(num2.value);
	
let diff = nm1 - nm2;
document.getElementById('result').innerHTML = diff;
document.getElementById('operation').innerHTML = "Subtraction";
});

multiplication.addEventListener("click", function(){
	let num1 = document.getElementById("n1");
	let nm1 = parseInt(num1.value);
	let num2 = document.getElementById("n2");
	let nm2 = parseInt(num2.value);
	
let mult = nm1 * nm2;
document.getElementById('result').innerHTML = mult;
document.getElementById('operation').innerHTML = "Multiplication";
});

division.addEventListener("click", function(){
	let num1 = document.getElementById("n1");
	let nm1 = parseInt(num1.value);
	let num2 = document.getElementById("n2");
	let nm2 = parseInt(num2.value);
	
let div = nm1 / nm2;
document.getElementById('result').innerHTML = div;
document.getElementById('operation').innerHTML = "Division";
});

modulo.addEventListener("click", function(){
	let num1 = document.getElementById("n1");
	let nm1 = parseInt(num1.value);
	let num2 = document.getElementById("n2");
	let nm2 = parseInt(num2.value);
	
let mod = nm1 % nm2;
document.getElementById('result').innerHTML = mod;
document.getElementById('operation').innerHTML = "Modulo";
});
